<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <title>Decks</title>
</head>
<body>

<header>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Flashcards</a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="home">Home</a></li>
                <li class="active"><a href="decks">Decks</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Flashcards
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/flashcards">My Flashcards</a></li>
                        <li><a href="/flashcards/public">Public Flashcards</a></li>
                    </ul>
                </li>
                <li><a href="#">About</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><span class="glyphicon glyphicon-user"></span><c:out value="${sessionScope.username}"/></a></li>
                <li><a href="/logout"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
            </ul>
        </div>
    </nav>
</header>
<div class="container">
    <div class="table-wrapper">
        <div class="table-title">
            <div class="row">
                <div class="col-sm-8"><h2>My decks</h2></div>
                <div class="col-sm-4">
                    <button type="button" class="btn btn-info add-new" id="addBtn"><i class="fa fa-plus">Add New</i></button>

                    <!-- Modal -->
                    <div class="modal fade" id="addModal" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 style="color:dodgerblue;"> New deck</h4>
                                </div>
                                <div class="modal-body">
                                    <form role="form" method="post" action="/decks/add">
                                        <div class="form-group">
                                            <label for="deckName">Name</label>
                                            <input type="text" class="form-control" id="deckName" name= "deckName" placeholder="Enter Deck name">
                                        </div>
                                        <div class="form-group">
                                            <label for="initFlashcards">Choose flashcards:</label>
                                            <select class="form-control" id="initFlashcards" name="initFlashcards" multiple>
                                                <c:forEach items="${requestScope.flashcards}" var="flashcard">
                                                    <option value="<c:out value="${flashcard.id}"/>"><c:out value="${flashcard.title}"/></option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-lg btn-primary btn-block">Create</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${requestScope.decks}" var="deck">
                <script>
                    $(document).ready(function(){
                        $("#editBtn<c:out value="${deck.id}"/>").click(function(){
                            $("#editModal<c:out value="${deck.id}"/>").modal();
                        });
                    });
                </script>
                <div class="modal fade" id="editModal<c:out value="${deck.id}"/>" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 style="color:dodgerblue;" >Rename</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form" method="post" action="/decks/update">
                                    <input hidden name="idDeck" value="<c:out value="${deck.id}"/>"/>
                                    <div class="form-group">
                                        <label for="newName">New name</label>
                                        <input type="text" class="form-control" id="newName" name= "newName" value="<c:out value="${deck.name}"/>">
                                    </div>
                                    <button type="submit" class="btn btn-lg btn-primary btn-block">Edit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <script>
                    $(document).ready(function(){
                        $("#addFlashcardsBtn<c:out value="${deck.id}"/>").click(function(){
                            $("#addFlashcardsModal<c:out value="${deck.id}"/>").modal();
                        });
                    });
                </script>
                <div class="modal fade" id="addFlashcardsModal<c:out value="${deck.id}"/>" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 style="color:dodgerblue;" > Add <c:out value="${flashcard.title}"/> to deck</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form" method="post" action="/flashcards/addToDeck">
                                    <input hidden name="deckId" value="<c:out value="${deck.id}"/>"/>
                                    <div class="form-group">
                                        <label for="flashcardId">Select flashcards:</label>
                                        <select class="form-control" id="flashcardId" name="flashcardId" multiple>
                                            <c:forEach items="${requestScope.flashcards}" var="flashcard">
                                                <option value="<c:out value="${flashcard.id}"/>"><c:out value="${flashcard.title}"/></option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-lg btn-primary btn-block">Add</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <tr>
                    <td><c:out value="${deck.name}"/></td>
                    <td>
                        <ul class="list-inline m-0">
                            <li class="list-inline-item">
                                <button class="btn btn-primary btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Add flashcards" id="addFlashcardsBtn<c:out value="${deck.id}"/>"><i class="fa fa-table"></i></button>
                            </li>
                            <li class="list-inline-item">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Edit" id="editBtn<c:out value="${deck.id}"/>"><i class="fa fa-edit"></i></button>

                            </li>
                            <li class="list-inline-item">
                                <form action="/decks/delete" method="post">
                                    <input type="hidden" name="deckId" value="<c:out value="${deck.id}"/>"/>
                                    <input type="hidden" name="redirectPath" value="/decks"/>
                                    <button type="submit" class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></button>
                                </form>
                            </li>
                        </ul>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
<footer class="bg-light text-center text-lg-start">
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
        &copy 2022 Copyright: Radoslaw Gredel
    </div>
</footer>
<script>
    $(document).ready(function(){
        $("#addBtn").click(function(){
            $("#addModal").modal();
        });
    });
</script>
</body>
</body>
</html>