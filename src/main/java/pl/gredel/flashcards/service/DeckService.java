package pl.gredel.flashcards.service;

import pl.gredel.flashcards.db.dao.DeckDAO;
import pl.gredel.flashcards.db.dao.util.DAOException;
import pl.gredel.flashcards.model.Deck;
import pl.gredel.flashcards.model.Users;
import pl.gredel.flashcards.service.util.ServiceException;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DeckService {
    private static final Logger LOGGER = Logger.getLogger( DeckService.class.getName() );
    private DeckDAO deckDAO = new DeckDAO();

    public int createDeck(String name, String username) throws ServiceException {
        UserService userService = new UserService();
        Users user = userService.getUserByLogin(username);
        Deck deck = new Deck(name,user);
        try {
            deck = deckDAO.create(deck);
            return deck.getId();
        } catch (DAOException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new ServiceException("Unexpected error! Cannot create deck.", e);
        }
    }

    public void addFlashcardToDeck(int flashcardID, int deckId) throws ServiceException {
        try {
            deckDAO.addFlashcardToDeck(flashcardID,deckId);
        } catch (DAOException e) {
            throw new ServiceException("Cannot add flashcard to deck",e);
        }
    }
    public List<Deck> getAllDecksByUsername(String username) throws ServiceException {

        UserService userService = new UserService();
        Users user = userService.getUserByLogin(username);

        try {
            List<Deck> decks = deckDAO.findAllByUserId(user.getId());
            return decks;
        } catch (DAOException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new ServiceException("Unexpected error.", e);
        }
    }

    public void updateDeck(int idDeck, String name, String username) throws ServiceException {
        UserService userService = new UserService();
        Users user = userService.getUserByLogin(username);
        Deck deck = new Deck(idDeck, name,user);
        try {
            deckDAO.update(deck);
        } catch (DAOException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new ServiceException("Unexpected error! Cannot update deck.", e);
        }
    }

    public void deleteDeck(int deckId) throws ServiceException {
        try {
            deckDAO.delete(deckId);
        } catch (DAOException e) {
        LOGGER.log(Level.SEVERE, e.toString(), e);
        throw new ServiceException("Unexpected error! Cannot delete deck.", e);
    }
    }
    public void addFlashcardsToDeck(int deckId, String[] flashcardIds){
        if(flashcardIds != null) {
            Arrays.stream(flashcardIds).filter(id -> id != "")
                    .map(Integer::parseInt)
                    .forEach(idFlashcard -> {
                        try {
                            addFlashcardToDeck(idFlashcard, deckId);
                        } catch (ServiceException e) {
                            LOGGER.log(Level.SEVERE, e.toString(), e);
                        }
                    });
        }
    }
}
