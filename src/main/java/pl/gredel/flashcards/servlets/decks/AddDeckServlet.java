package pl.gredel.flashcards.servlets.decks;

import pl.gredel.flashcards.db.dao.DeckDAO;
import pl.gredel.flashcards.model.Deck;
import pl.gredel.flashcards.service.DeckService;
import pl.gredel.flashcards.service.FlashcardService;
import pl.gredel.flashcards.service.util.ServiceException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@WebServlet("/decks/add")
public class AddDeckServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger( AddDeckServlet.class.getName() );

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String name = req.getParameter("deckName");
        String[] initFlashcards = req.getParameterValues("initFlashcards");
        String username = req.getSession().getAttribute("username").toString();

        DeckService deckService = new DeckService();

        try {
            int deckId= deckService.createDeck(name, username);
            deckService.addFlashcardsToDeck(deckId , initFlashcards);
        } catch (ServiceException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            String failureMessage = e.getMessage();
            req.setAttribute("error",failureMessage);
        }

        resp.sendRedirect("/decks");

    }
}
