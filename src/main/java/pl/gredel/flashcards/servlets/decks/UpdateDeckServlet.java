package pl.gredel.flashcards.servlets.decks;

import pl.gredel.flashcards.service.DeckService;
import pl.gredel.flashcards.service.FlashcardService;
import pl.gredel.flashcards.service.util.ServiceException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet("/decks/update")
public class UpdateDeckServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger( UpdateDeckServlet.class.getName() );

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String username = req.getSession().getAttribute("username").toString();
        String name = req.getParameter("newName");
        int idDeck = Integer.parseInt(req.getParameter("idDeck"));


        DeckService deckService = new DeckService();

        try {
            deckService.updateDeck(idDeck,name, username);
            resp.sendRedirect("/decks");
        } catch (ServiceException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            resp.sendRedirect("/decks");
        }
    }
}
