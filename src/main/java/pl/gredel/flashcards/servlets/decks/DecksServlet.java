package pl.gredel.flashcards.servlets.decks;

import pl.gredel.flashcards.model.Deck;
import pl.gredel.flashcards.service.DeckService;
import pl.gredel.flashcards.service.util.ServiceException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


@WebServlet("/decks")
public class DecksServlet  extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger( DecksServlet.class.getName() );

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getSession().getAttribute("username").toString();
        DeckService deckService = new DeckService();
        try {
            List<Deck> decks = deckService.getAllDecksByUsername(username);
            req.setAttribute("decks", decks);
        } catch (ServiceException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
        }
        req.getRequestDispatcher("/html/decks.jsp").forward(req, resp);
    }
}