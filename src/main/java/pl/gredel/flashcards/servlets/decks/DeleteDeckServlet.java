package pl.gredel.flashcards.servlets.decks;

import pl.gredel.flashcards.service.DeckService;
import pl.gredel.flashcards.service.FlashcardService;
import pl.gredel.flashcards.service.util.ServiceException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet("/decks/delete")
public class DeleteDeckServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger( DeleteDeckServlet.class.getName() );
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
       int deckId = Integer.parseInt(req.getParameter("flashcardId"));
       DeckService deckService = new DeckService();

        try {
            deckService.deleteDeck(deckId);
            resp.sendRedirect("/decks");
        } catch (ServiceException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            resp.sendRedirect("/decks");
        }
    }
}
