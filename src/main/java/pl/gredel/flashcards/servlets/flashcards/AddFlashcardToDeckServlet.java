package pl.gredel.flashcards.servlets.flashcards;

import pl.gredel.flashcards.service.DeckService;
import pl.gredel.flashcards.service.FlashcardService;
import pl.gredel.flashcards.service.util.ServiceException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@WebServlet("/flashcards/addToDeck")
public class AddFlashcardToDeckServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(AddFlashcardToDeckServlet.class.getName());

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        int deckId = Integer.parseInt(req.getParameter("deckId"));
        String[] flashcardsIds = req.getParameterValues("flashcardId");
        String redirectPath = req.getParameter("redirectPath");
        DeckService deckService = new DeckService();

        deckService.addFlashcardsToDeck(deckId,flashcardsIds);

        if (redirectPath == null) {
            resp.sendRedirect("/flashcards");
        }else{
            resp.sendRedirect(redirectPath);
        }
    }
}