package pl.gredel.flashcards.filters;

import pl.gredel.flashcards.model.Category;
import pl.gredel.flashcards.model.Deck;
import pl.gredel.flashcards.model.Flashcard;
import pl.gredel.flashcards.service.CategoryService;
import pl.gredel.flashcards.service.DeckService;
import pl.gredel.flashcards.service.FlashcardService;
import pl.gredel.flashcards.service.util.ServiceException;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebFilter("/*")
public class DecksFilter implements Filter {
    private static Logger LOGGER = Logger.getLogger(DecksFilter.class.getName() );

    @Override
    public void destroy() {}

    @Override
    public void init(FilterConfig arg0){}

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest)req;
        if(request.getRequestURI().startsWith("/decks")){
            HttpSession session = request.getSession();

            FlashcardService flashcardService = new FlashcardService();
            try {
                String username = session.getAttribute("username").toString();
                List<Flashcard> flashcards = flashcardService.getAllFlashcardsByUsername(username);
                req.setAttribute("flashcards", flashcards);
            } catch (ServiceException e) {
                LOGGER.log(Level.SEVERE, e.toString(), e);
            }
        }

        chain.doFilter(request, res);
    }

}
